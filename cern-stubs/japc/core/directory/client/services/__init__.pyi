from typing import ClassVar as _py_ClassVar
import java.util


class BindInstance:
    globalList: _py_ClassVar[java.util.HashMap] = ...
    def __init__(self): ...
    def checkExisting(self) -> bool: ...
    def getServerInfo(self) -> java.util.Properties: ...
    def toString(self) -> str: ...
