from typing import Any as _py_Any
from typing import List as _py_List
from typing import ClassVar as _py_ClassVar
from typing import overload
import java.io
import java.lang
import javax.print_
import javax.print_.attribute


class PageFormat(java.lang.Cloneable):
    LANDSCAPE: _py_ClassVar[int] = ...
    PORTRAIT: _py_ClassVar[int] = ...
    REVERSE_LANDSCAPE: _py_ClassVar[int] = ...
    def __init__(self): ...
    def clone(self) -> _py_Any: ...
    def getHeight(self) -> float: ...
    def getImageableHeight(self) -> float: ...
    def getImageableWidth(self) -> float: ...
    def getImageableX(self) -> float: ...
    def getImageableY(self) -> float: ...
    def getMatrix(self) -> _py_List[float]: ...
    def getOrientation(self) -> int: ...
    def getPaper(self) -> 'Paper': ...
    def getWidth(self) -> float: ...
    def setOrientation(self, int: int) -> None: ...
    def setPaper(self, paper: 'Paper') -> None: ...

class Pageable:
    UNKNOWN_NUMBER_OF_PAGES: _py_ClassVar[int] = ...
    def getNumberOfPages(self) -> int: ...
    def getPageFormat(self, int: int) -> PageFormat: ...
    def getPrintable(self, int: int) -> 'Printable': ...

class Paper(java.lang.Cloneable):
    def __init__(self): ...
    def clone(self) -> _py_Any: ...
    def getHeight(self) -> float: ...
    def getImageableHeight(self) -> float: ...
    def getImageableWidth(self) -> float: ...
    def getImageableX(self) -> float: ...
    def getImageableY(self) -> float: ...
    def getWidth(self) -> float: ...
    def setImageableArea(self, double: float, double2: float, double3: float, double4: float) -> None: ...
    def setSize(self, double: float, double2: float) -> None: ...

class Printable:
    PAGE_EXISTS: _py_ClassVar[int] = ...
    NO_SUCH_PAGE: _py_ClassVar[int] = ...

class PrinterException(java.lang.Exception):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, string: str): ...

class PrinterGraphics:
    def getPrinterJob(self) -> 'PrinterJob': ...

class PrinterJob:
    def __init__(self): ...
    def cancel(self) -> None: ...
    @overload
    def defaultPage(self, pageFormat: PageFormat) -> PageFormat: ...
    @overload
    def defaultPage(self) -> PageFormat: ...
    def getCopies(self) -> int: ...
    def getJobName(self) -> str: ...
    def getPageFormat(self, printRequestAttributeSet: javax.print_.attribute.PrintRequestAttributeSet) -> PageFormat: ...
    def getPrintService(self) -> javax.print_.PrintService: ...
    @classmethod
    def getPrinterJob(cls) -> 'PrinterJob': ...
    def getUserName(self) -> str: ...
    def isCancelled(self) -> bool: ...
    @classmethod
    def lookupPrintServices(cls) -> _py_List[javax.print_.PrintService]: ...
    @classmethod
    def lookupStreamPrintServices(cls, string: str) -> _py_List[javax.print_.StreamPrintServiceFactory]: ...
    @overload
    def pageDialog(self, pageFormat: PageFormat) -> PageFormat: ...
    @overload
    def pageDialog(self, printRequestAttributeSet: javax.print_.attribute.PrintRequestAttributeSet) -> PageFormat: ...
    @overload
    def printDialog(self) -> bool: ...
    @overload
    def printDialog(self, printRequestAttributeSet: javax.print_.attribute.PrintRequestAttributeSet) -> bool: ...
    def setCopies(self, int: int) -> None: ...
    def setJobName(self, string: str) -> None: ...
    def setPageable(self, pageable: Pageable) -> None: ...
    def setPrintService(self, printService: javax.print_.PrintService) -> None: ...
    @overload
    def setPrintable(self, printable: Printable) -> None: ...
    @overload
    def setPrintable(self, printable: Printable, pageFormat: PageFormat) -> None: ...
    def validatePage(self, pageFormat: PageFormat) -> PageFormat: ...

class Book(Pageable):
    def __init__(self): ...
    @overload
    def append(self, printable: Printable, pageFormat: PageFormat) -> None: ...
    @overload
    def append(self, printable: Printable, pageFormat: PageFormat, int: int) -> None: ...
    def getNumberOfPages(self) -> int: ...
    def getPageFormat(self, int: int) -> PageFormat: ...
    def getPrintable(self, int: int) -> Printable: ...
    def setPage(self, int: int, printable: Printable, pageFormat: PageFormat) -> None: ...

class PrinterAbortException(PrinterException):
    @overload
    def __init__(self): ...
    @overload
    def __init__(self, string: str): ...

class PrinterIOException(PrinterException):
    def __init__(self, iOException: java.io.IOException): ...
    def getCause(self) -> java.lang.Throwable: ...
    def getIOException(self) -> java.io.IOException: ...
